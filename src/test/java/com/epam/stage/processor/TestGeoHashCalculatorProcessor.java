/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.epam.stage.processor;

import _ss_com.streamsets.datacollector.validation.Issue;
import com.streamsets.pipeline.api.Field;
import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.api.Stage.ConfigIssue;
import com.streamsets.pipeline.api.StageException;
import com.streamsets.pipeline.sdk.ProcessorRunner;
import com.streamsets.pipeline.sdk.RecordCreator;
import com.streamsets.pipeline.sdk.StageRunner;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

public class TestGeoHashCalculatorProcessor {

  @Test
  public void testHashSizeOutOfRange() throws StageException {

    ProcessorRunner runner = new ProcessorRunner.Builder(GeoHashCalculator.class)
      .addConfiguration("longitudeFieldPath", "/Latitude")
      .addConfiguration("latitudeFieldPath", "/Longitude")
      .addConfiguration("hashSize", 15)
      .addConfiguration("outputFieldPath", "/hash")
      .addOutputLane("output")
      .build();

    try {
      List<ConfigIssue> issues = runner.runValidateConfigs();

      Assert.assertTrue(issues.size() > 0);
      Assert.assertEquals(((Issue) issues.get(0)).getConfigName(), ConfigParameters.HASH_SIZE.getMsg());
    } finally {
      runner.runDestroy();
    }
  }

  @Test
  public void testFlowWithoutField() throws StageException {
    ProcessorRunner runner = new ProcessorRunner.Builder(GeoHashCalculator.class)
      .addConfiguration("longitudeFieldPath", "/Latitude")
      .addConfiguration("latitudeFieldPath", "/Longitude")
      .addConfiguration("hashSize", 5)
      .addConfiguration("outputFieldPath", "/hash")
      .addOutputLane("output")
      .build();

    try {
      runner.runInit();
      Record record = RecordCreator.create();
      LinkedHashMap<String, Field> params = new LinkedHashMap<>();
      params.put("Latitude", Field.create(20));

      record.set("/", Field.createListMap(params));
      StageRunner.Output output = runner.runProcess(Collections.singletonList(record));

      Assert.assertEquals(0, output.getRecords().get("output").size());
    } finally {
      runner.runDestroy();
    }
  }

  @Test
  public void testMistakesGoToErrorSinkAlongsideToCorrectCalculations() throws StageException {
    ProcessorRunner runner = new ProcessorRunner.Builder(GeoHashCalculator.class)
      .addConfiguration("longitudeFieldPath", "/Latitude")
      .addConfiguration("latitudeFieldPath", "/Longitude")
      .addConfiguration("hashSize", 6)
      .addConfiguration("outputFieldPath", "/hash")
      .addOutputLane("output")
      .build();

    try {
      Assert.assertTrue(runner.runValidateConfigs().isEmpty());

      runner.runInit();

      Record normalRecord = RecordCreator.create();
      LinkedHashMap<String, Field> params = new LinkedHashMap<>();
      params.put("Latitude", Field.create(20));
      params.put("Longitude", Field.create(20));
      normalRecord.set("/", Field.createListMap(params));

      Record recWithoutLongitude = RecordCreator.create();
      LinkedHashMap<String, Field> params2 = new LinkedHashMap<>();
      params2.put("Latitude", Field.create(20));
      params2.put("Longi", Field.create(20));
      recWithoutLongitude.set("/", Field.createListMap(params2));

      Record recWithBrokenLatitudeValue = RecordCreator.create();
      LinkedHashMap<String, Field> params3 = new LinkedHashMap<>();
      params3.put("Latitude", Field.create("sss"));
      params3.put("Longitude", Field.create(20));
      recWithBrokenLatitudeValue.set("/", Field.createListMap(params3));

      Record recWithBrokenLongitudeValue = RecordCreator.create();
      LinkedHashMap<String, Field> params4 = new LinkedHashMap<>();
      params4.put("Latitude", Field.create(20));
      params4.put("Longitude", Field.create("zz"));
      recWithBrokenLongitudeValue.set("/", Field.createListMap(params4));

      StageRunner.Output output = runner.runProcess(Arrays.asList(
        normalRecord,
        recWithoutLongitude,
        recWithBrokenLatitudeValue,
        recWithBrokenLongitudeValue
      ));

      Assert.assertEquals(3, runner.getErrorRecords().size());
      Assert.assertEquals(1, output.getRecords().get("output").size());

      Assert.assertEquals("s7w1z0", output.getRecords().get("output").get(0).get("/hash").getValueAsString());

    } finally {
      runner.runDestroy();
    }
  }

  @Test
  public void testGeoHashCalculation() throws StageException {
    ProcessorRunner runner = new ProcessorRunner.Builder(GeoHashCalculator.class)
      .addConfiguration("longitudeFieldPath", "/Latitude")
      .addConfiguration("latitudeFieldPath", "/Longitude")
      .addConfiguration("hashSize", 6)
      .addConfiguration("outputFieldPath", "/hash")
      .addOutputLane("output")
      .build();

    try {
      Assert.assertTrue(runner.runValidateConfigs().isEmpty());
      runner.runInit();

      Record record = RecordCreator.create();
      LinkedHashMap<String, Field> params = new LinkedHashMap<>();
      params.put("Latitude", Field.create(20));
      params.put("Longitude", Field.create(20));

      record.set("/", Field.createListMap(params));

      StageRunner.Output output = runner.runProcess(Collections.singletonList(record));
      Assert.assertEquals(1, output.getRecords().get("output").size());
      Assert.assertEquals("s7w1z0", output.getRecords().get("output").get(0).get("/hash").getValueAsString());
    } finally {
      runner.runDestroy();
    }
  }
}
