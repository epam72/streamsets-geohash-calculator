/**
 * Copyright 2015 StreamSets Inc.
 * <p>
 * Licensed under the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.epam.stage.processor;

import ch.hsr.geohash.GeoHash;
import com.epam.stage.lib.Errors;
import com.streamsets.pipeline.api.Field;
import com.streamsets.pipeline.api.Record;
import com.streamsets.pipeline.api.StageException;
import com.streamsets.pipeline.api.base.SingleLaneRecordProcessor;
import com.streamsets.pipeline.api.service.dataformats.DataParserException;

import java.util.List;

public abstract class GeoHashCalculatorProcessor extends SingleLaneRecordProcessor {

  public abstract String getLatitudeFieldPath();

  public abstract String getLongitudeFieldPath();

  public abstract Integer getHashSize();

  public abstract String getOutputFieldPath();

  /** {@inheritDoc} */
  @Override
  protected List<ConfigIssue> init() {
    List<ConfigIssue> issues = super.init();

    if (getHashSize() < 1 || getHashSize() > 8) {
      issues.add(getContext().createConfigIssue(Groups.CONFIG.name(), ConfigParameters.HASH_SIZE.getMsg(), Errors.HASH_SIZE, "Hash size must be within range [1;8]"));
    }

    return issues;
  }

  /** {@inheritDoc} */
  @Override
  public void destroy() {
    super.destroy();
  }

  /** {@inheritDoc} */
  @Override
  protected void process(Record record, SingleLaneBatchMaker batchMaker) throws StageException {

    try {
      if (!record.has(getLatitudeFieldPath())) throw new DataParserException(Errors.LATITUDE_FIELD_NOT_FOUND);
      if (!record.has(getLongitudeFieldPath())) throw new DataParserException(Errors.LONGITUDE_FIELD_NOT_FOUND);

      double lat;
      double lon;

      try {
        lat = record.get(getLatitudeFieldPath()).getValueAsDouble();
      } catch (Exception ex) {
        throw new DataParserException(Errors.WRONG_LATITUDE_VALUE, record.get(getLatitudeFieldPath()).getValue());
      }

      try {
        lon = record.get(getLongitudeFieldPath()).getValueAsDouble();
      } catch (Exception ex) {
        throw new DataParserException(Errors.WRONG_LONGITUDE_VALUE, record.get(getLongitudeFieldPath()).getValue());
      }

      record.set(getOutputFieldPath(), Field.create(GeoHash.withCharacterPrecision(lat, lon, getHashSize()).toBase32()));
      batchMaker.addRecord(record);

    } catch (Exception ex) {
      getContext().toError(record, ex);
    }
  }

}