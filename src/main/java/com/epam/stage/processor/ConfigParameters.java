package com.epam.stage.processor;

public enum ConfigParameters {
    HASH_SIZE("Hash size"),
    ;

    private final String msg;

    ConfigParameters(String msg) {
     this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
